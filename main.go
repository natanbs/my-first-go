package main

import (
	"fmt"
	"log"
	"net/http"
	"os/exec"
)

func toast(w http.ResponseWriter, req *http.Request) {
	out, err := exec.Command("/usr/sbin/hping3", "-1", "--fast", "ynet.co.il" ).Output()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(w, "Toasting")
	fmt.Printf("Toasting\n %s\n", out)
}


func kill(w http.ResponseWriter, req *http.Request) {
	fmt.Println("Killing toaster")
	exec.Command("/usr/bin/pkill", "-9", "hping3").Output()
}

func main() {
	http.HandleFunc("/toast", toast)
	http.HandleFunc("/kill", kill)
	fmt.Println("Starting server...")
	http.ListenAndServe(":8090", nil)
}
